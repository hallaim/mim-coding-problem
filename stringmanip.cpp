// stringmanip.cpp

#ifndef STRING_MANIP_C
#define STRING_MANIP_C

#include "stringmanip.hpp"

#include <cstdlib>
#include <iostream>
#include <string>
#include <vector>



void stringmanip::push(std::string s){
	str_list.push_back(s);	// adds a string to the list/vector
}

void stringmanip::manip(){

	// Counts total chars before changing strings
	for(int i = 0; i < str_list.size(); i++)	// for each string
		inCount += str_list[i].length();

	// Reverse the string if it is a multiple of 4
	for(int i = 0; i < str_list.size(); i++)	// for each string
		if (str_list[i].length() % 4 == 0)		// if length is a multiple of 4
			str_list[i] = reverse(str_list[i]); // reverse string

	// Truncate the string to 5 characters if its length is a multiple of 5
	for(int i = 0; i < str_list.size(); i++)	// for each string
		if (str_list[i].length() % 5 == 0)		// if length is a multiple of 5
			str_list[i].resize(5);

	// Convert to all uppercase if it contains at least 3 uppercase in the first 5 letters.
	for(int i = 0; i < str_list.size(); i++){	// for each string
		int upper_count = 0;	// counts number of uppercase letters
		for(int j = 0; (j<str_list[i].length()) && (j<5); j++)
			if((str_list[i][j]>=65) && (str_list[i][j]<=90))
				upper_count++;
		if(upper_count >=3)		//then change to all uppercase
			for(int j = 0; (j<str_list[i].length()); j++)	// for each character
				str_list[i][j] = toupper(str_list[i][j]);
	}

	// If the string ends with a hyphen, remove it and append the next string in the list to the current one
	for(int i = 0; i < str_list.size(); i++)	// for each string
		if (str_list[i][str_list[i].length()-1] == '-'){		// if it ends in a '-'
			str_list[i].resize(str_list[i].length()-1);		// remove hyphen
			if (i != str_list.size()-1)   // add next string to current string if next string exists
				str_list[i] = str_list[i] + str_list[i+1];
		}

	// print out changed strings and count characters in the output
	for(int i = 0; i < str_list.size(); i++){
		std::cout << str_list[i] << std::endl;
		outCount += str_list[i].length();
	}

	// print out report
	std::cout << "\nTotal characters in the input: " << inCount
			  << "\nTotal characters in the ouput: " << outCount << std::endl;
	//		  << "\nMedian length of all strings:  " << median   // did not finish the median
}


// reverse string function
std::string reverse(std::string s){
	std::string result = ""; 		//create a new empty string
	for (int i=0; i<s.length(); i++){
		result = s[i] + result; //take the newest character and add it at the beginning
	}
	return result;	// returns reversed string
} 

#endif