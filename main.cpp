// main.cpp
// 10/12/14

#include "stringmanip.cpp"

#include <iostream>
#include <cstdlib>
#include <vector>
#include <string>

int main()
{
	stringmanip *list = new stringmanip; // instance of class

	list->push("reverse!");		// test strings
	list->push("test");			// test the reverse
	list->push("not_reverse");

	list->push("truncate..");	// tests truncate
	list->push("again..truncate");
	list->push("should not truncate");

	list->push("Uppercase");	// tests uppercase
	list->push("UPpercase");
	list->push("UPpercASE");
	list->push("UPpErcase");

	list->push("-hyphen");		// tests hyphen removal
	list->push("hyphenated-");
	list->push("word.");
	list->push("");

	list->push("abcdefghijklmnopqrs-"); // reverse then truncate
	list->push("-abcdefghijklmno");	// reverse then remove '-'
	list->push("_alphabet");

	list->manip();	// manipulate then print out the strings in the list
					// total of 171 characters in the input list

 	return 0;
}