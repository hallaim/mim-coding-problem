// stringmanip.hpp

#ifndef STRING_MANIP_H
#define STRING_MANIP_H

#include <cstdlib>
#include <iostream>
#include <string>
#include <vector>


// function prototype, used in manip()
std::string reverse(std::string);


class stringmanip{
	private: 
		std::vector<std::string> str_list;

		int inCount, outCount, median;  // total chars in input, output, and median length

	public:
		stringmanip(){ // default class constructor
			inCount=0; outCount=0; median=0;
		}; 

		void push(std::string); // adds string to list/vector

		void manip(); // manipulates strings and prints them out
};

#endif